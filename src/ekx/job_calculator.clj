(ns ekx.job-calculator
  (:require [clojure.string :as string]
            [clojure.set]
            [clojure.core.reducers :as r]))


(defn foo [bar] bar)

(defn make-item ([name cost & opts]
                 {:name name :cost cost :opts (set (filter keyword? opts))}))

(defn enhance-item [item & opts] (update-in item [:opts] #(clojure.set/union % opts)))

(defn parse-line [line]
  (condp re-matches line
      #"extra-margin" :extra-margin
      #"([^\p{Space}]+) ([0-9.]+)(?: (exempt))?"
      :>> (fn [matches]
            (let [[all name amt ex] matches]
              (make-item name (read-string amt) (keyword ex))))
      #"[\p{Space}]+" nil))

(defn divide [& args]
  (vector (apply filter args) (apply remove args)))

(defn parse-job [jobstr]
  (let [entries (->> (string/split-lines jobstr)
                     (map string/trim)
                     (map parse-line)
                     (remove nil?))
        [kwds items] (divide keyword? entries)]
    (map #(apply enhance-item % kwds) items)
    ;; (vector kwds items)
    ))

(defn round-to-nearest
  ([frac num]
   (* frac (Math/round (/ num frac))))
  ([frac]
   (partial round-to-nearest frac)))

(defn mkcalc [default-margin add-margin tax]
  (fn [item]
    (let [{:keys [name cost opts]} item
          opts (set opts)
          margin (+ default-margin (if (opts :extra-margin) add-margin 0))
          tax (if (opts :exempt) 0 tax)]
      (vector name (round-to-nearest 0.01 (* cost (+ 1 tax))) (* cost margin)))))

(def calc-item (mkcalc 0.11 0.05 0.07))

(defn calc-job [job-str]
       (let [entries (mapv calc-item (parse-job job-str))
             total (round-to-nearest 0.02 (reduce #(+ %1 (apply + (rest %2))) 0 entries))]
         (conj entries (vector "total" total))))

(defn fmt-entry [entry] (apply format "%s: $%.2f\n" entry))

(def fmt-entries (fn [entries] (->> entries (map fmt-entry) (reduce str))))

(def fmt-job (comp fmt-entries calc-job))


(def job "extra-margin
frisbees 19385.38 exempt
yo-yos 1829 exempt")

(fmt-entries (calc-job job))
