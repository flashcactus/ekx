(ns ekx.job-calculator-test
  (:require [clojure.test :refer :all]
            [ekx.job-calculator :refer :all]
            [clojure.string :as str]))

(def testpairs [
                [
                 "extra-margin
envelopes 520.00
letterhead 1983.37 exempt"
                 "envelopes: $556.40
letterhead: $1983.37
total: $2940.30"]

                [
                 "t-shirts 294.04"
                 "t-shirts: $314.62
total: $346.96"
                 ]
                ["extra-margin
frisbees 19385.38 exempt
yo-yos 1829 exempt"
                 "frisbees: $19385.38
yo-yos: $1829.00
total: $24608.68"]])


(defn normalize [output] (str/trim output))
(defn testpair [in out] (is (= (normalize (fmt-job in)) (normalize out))))

(deftest inout-test
  (testing "tz-cases"
    (mapv #(apply testpair %) testpairs)))
